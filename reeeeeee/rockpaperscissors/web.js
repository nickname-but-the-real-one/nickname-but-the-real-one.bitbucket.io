const userScore= 0 ;
const computerScore = 0;
const userScore_span = document.getElementById("user-score");
const computerScore_span = document.getElementById("computer-score");
const soreBoard_div = document.querySelector(".score-board");
const result_div = document.querySelector(".result");
const resultMessage_p = document.getElementById("resultMessage");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");

function getComputerChoice() {
	const choices = ['r','p','s'];
	const randomNumber = Math.floor(Math.random() * 3);
	return choices[randomNumber];
	
}
var userWins = 0;
var compWins = 0;
function game(userChoice) {
	const computerChoice = getComputerChoice();
	switch (userChoice + computerChoice) {
		case "rs":
		case "pr":
		case "sp":
			console.log ("user wins");
			resultMessage_p.innerHTML="user wins but computer will kill you someday, someday...";
			userWins = userWins+2;
			userScore_span.innerHTML = userWins;
			break;
		case "rr":
		case "pp":
		case "ss":
			console.log ("IT'S A TIE");
			resultMessage_p.innerHTML="YOU TIED, so this is how its gonna be huh.";
			break;
		default:
			compWins = compWins+1;	
			computerScore_span.innerHTML = compWins;
			console.log ("computer sucks, but he/she still won");
			resultMessage_p.innerHTML="you will die someday, and it will be because of meeeeeee";
			break;
	}
	console.log("userChoice =>" + userChoice);
	console.log("computerChoice =>" + computerChoice)
}

function main() {
	rock_div.addEventListener('click', function() {
		game("r");
		
	});
	paper_div.addEventListener('click', function() {
		game("p");
		
	});
	scissors_div.addEventListener('click', function() {
		game("s");
		
	});
}

main();